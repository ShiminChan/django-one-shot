from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)

def show_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "todos_detail": details,
    }
    return render(request, "todos/detail.html", context)

def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList= form.save()
            return redirect("todo_list_detail", id=TodoList.id)
    else:
        form = TodoListForm()
 
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def edit_todolist(request, id):
    details = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=details)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=details)
    context = {
        "todos_detail": details,
        "form": form,
    } 
    return render(request, "todos/edit.html", context)  

def delete_todolist(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
        return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    
    context = {
        "todoitem_form": form,
    }
    return render(request, "todos/create_item.html", context)
    
def edit_todoitem(request, id):
    each_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=each_item)
        if form.is_valid():
            each_item = form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=each_item)
    context = {
        "todos_detail": each_item,
        "todoitem_form": form,
    }
    return render(request, "todos/edit_item.html", context)