from django.urls import path
from todos.views import todo_list_list, show_detail, create_todolist, edit_todolist, delete_todolist, create_todoitem, edit_todoitem

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", show_detail, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/edit/", edit_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("items/create/", create_todoitem, name="todo_item_create"),
    path("items/<int:id>/edit/", edit_todoitem, name="todo_item_update"),
]